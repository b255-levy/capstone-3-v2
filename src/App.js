import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import LoginPage from './pages/Login';
import Logout from './pages/Logout';
import RegisterPage from './pages/Register';
import AdminDashboard from './pages/AdminDashboard';
import UserProducts from './pages/UserProducts';
import AppNavBar from './pages/Navbar';
import UserContext from './pages/UserContext';
import HomePage from './pages/Home'
import './App.css';

function App() {
  const [user, setUser] = useState({ id: null, isAdmin: false });

  return (
    <UserContext.Provider value={{ user, setUser }}>
      <Router>
        <div className="App">
          <AppNavBar /> {}
          <Routes>
            <Route path="/home" element={<HomePage />} />
            <Route path="/login" element={<LoginPage />} />
            <Route path="/register" element={<RegisterPage />} />
            <Route path="/admin" element={<AdminDashboard />} />
            <Route path="/products" element={<UserProducts />} />
            <Route path="/logout" element={<Logout />} />
          </Routes>
        </div>
      </Router>
    </UserContext.Provider>
  );
}

export default App;
