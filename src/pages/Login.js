import React, { useState, useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from './UserContext';

function LoginPage() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const { user, setUser } = useContext(UserContext);

  useEffect(() => {
    console.log(user);
  }, [user]);


  const handleFormSubmit = async (event) => {
    event.preventDefault();
    console.log('Submit button pressed');
    try {
      const response = await fetch('https://capstone-2-levy.onrender.com/users/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, password }),
      });

      if (!response.ok) {
        throw new Error(`Error ${response.status}: ${response.statusText}`);
      }

      const data = await response.json();

      // Log the parsed JSON data
      console.log('Data received', data); 

      if (data && data.token) {
        // Store token in local storage
        localStorage.setItem('authToken', data.token);

        // Fetch user details
        const userResponse = await fetch('https://capstone-2-levy.onrender.com/users/details', {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data.token}`
          },
        });

        if (!userResponse.ok) {
          throw new Error(`Error ${userResponse.status}: ${userResponse.statusText}`);
        }

        const userData = await userResponse.json();

        // Update user context with ID and admin status
        setUser({ id: userData._id, isAdmin: userData.isAdmin, token: data.token });

        window.alert('Successfully logged in!');
      } else {
        throw new Error('Unexpected response data');
      }
    } catch (error) {
      console.error('Error occurred', error); 
      setError(error.message);
    }
  };




  if (isLoggedIn) {
    return <Navigate to="/products" />;
  }

  return (
    <div>
      <h2>Login Page</h2>
      <form onSubmit={handleFormSubmit}>
        <label>
          Email:
          <input
            type="email"
            name="email"
            value={email}
            onChange={(event) => setEmail(event.target.value)}
            required
          />
        </label>
        <br />
        <label>
          Password:
          <input
            type="password"
            name="password"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
            required
          />
        </label>
        <br />
        <button type="submit">Login</button>
      </form>
      {error && <p>{error}</p>}
    </div>
  );
}

export default LoginPage;
